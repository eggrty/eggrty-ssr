# eggrty-cli

用于快速创建一个Egg + React + SSR 应用的脚手架

## Getting Start

如何使用

```
$ npm install eggrty-cli -g
$ eggrty-cli init <Your Project Name>
$ cd <Your Project Name>
$ npm i
$ npm start
$ open http://localhost:8080
```

## Guide
