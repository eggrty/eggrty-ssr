import * as fs from 'fs'
import * as path from 'path'

function mkdirs (filePath: string) {
  if (fs.existsSync(filePath)) {
    return true
  }
  mkdirs(path.dirname(filePath))
  fs.mkdirSync(filePath)
  return true
}

function mv (oldPath: string, newPath: string) {
  mkdirs(path.dirname(newPath))
  return new Promise((res, rej) => {
    fs.rename(oldPath, newPath, function (err) {
      if (err) {
        if (err.code === 'EXDEV') {
          copy()
        } else {
          rej(err)
        }
        return
      }
      res()
    })

    function copy () {
      const readStream = fs.createReadStream(oldPath)
      const writeStream = fs.createWriteStream(newPath)

      readStream.on('error', rej)
      writeStream.on('error', rej)

      readStream.on('close', function () {
        fs.unlink(oldPath, res)
      })
      readStream.pipe(writeStream)
    }
  })
}

export {
  mkdirs,
  mv
}
