import { resolve } from 'path'
import { Readable } from 'stream'
import React from 'react'
import { renderToNodeStream, renderToString as reactRenderToString } from 'react-dom/server'
import { Config } from '../interface/config'

const resolveDir = (path: string) => resolve(process.cwd(), path)

const logGreen = (text: string) => {
  console.log(`\x1B[32m ${text}`)
}

const reactToStream = (Component: React.FunctionComponent, props: object, config: Config) => {
  const { baseDir } = config
  const BASE_DIR = baseDir || process.cwd()
  if (config.useReactToString) {
    return reactRenderToString(React.createElement(Component, props))
  } else {
    if (config.isRax) {
      const renderToString = require(BASE_DIR + '/node_modules/rax-server-renderer').renderToString
      return renderToString(React.createElement(Component, props))
    } else {
      return renderToNodeStream(React.createElement(Component, props))
    }
  }
}

const getVersion = (str: string) => {
  try {
    const arr = /\d+(\.\d+)+/.exec(str)
    if (arr === null) {
      throw new Error(str)
    }
    return arr[0]
  } catch (error) {
    console.error(`请检查cdn地址是否符合规范${str}`)
  }
}

class ReadableString extends Readable {
  str: string
  sent: boolean

  constructor (str: string) {
    super()
    this.str = str
    this.sent = false
  }

  _read () {
    if (!this.sent) {
      this.push(Buffer.from(this.str))
      this.sent = true
    } else {
      this.push(null)
    }
  }
}

class UtilsMath {
    /**
     * 加法，传入需要相加的数组成的数组
     * 此方法主要用于解决小数相加精度的问题
     * @export
     * @param {number[]} numList
     * @returns {number}
     */
  add (numList: number[]): number {
    if (numList.length === 1) {
      return numList[0]
    }
    let maxDecimalsLen = 0
    for (const tempNum of numList) {
      const decimalsLen = tempNum.toString().split('.')[1].length
      maxDecimalsLen = Math.max(maxDecimalsLen, decimalsLen)
    }
    const tempPow = Math.pow(10, maxDecimalsLen)
    let tempSum = 0
    for (const tempNum of numList) {
      tempSum = tempSum + tempNum * tempPow
    }
    return Number((tempSum / tempPow).toFixed(maxDecimalsLen))
  }
}

class ValidateUtil {
  validateNotEmpty (value: string, errMsg: string): string | void {
    if (value.trim().length < 1) {
      return errMsg
    }
  }

  validateMaxLength (value: string, maxLength: number, errMsg: string): string | void {
    if (value.trim().length > maxLength) {
      return errMsg
    }
  }

  validateMinLength (value: string, minLength: number, errMsg: string): string | void {
    if (value.trim().length < minLength) {
      return errMsg
    }
  }

  validateRegexp (value: string, regexp: string, errMsg: string): string | void {
    let rep = new RegExp(regexp)
    if (!rep.test(value)) {
      return errMsg
    }
  }

  validateMultiRegexp (value: string, regexps: string[], errMsg: string): string | void {
    return regexps.map((exp) => {
      if (!(RegExp(exp)).test(value)) { return errMsg }
    }).join('\n')
  }

  validatePhone (value: string): string | void {
    let exp = `^1[0-9]{10}$`
    return this.validateRegexp(value,exp,'手机号码格式不正确')
  }

  validateEmail (value: string): string | void {
    let exp = `^[\\w!#$%&'*+/=?^_\`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_\`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?$`
    return this.validateRegexp(value,exp,'电子邮箱格式不正确')
  }

  validateName (value: string): string | void {
    let exp = '^[\u4e00-\u9fa5]{1,10}$'
    return this.validateRegexp(value,exp,'姓名格式不正确')
  }

  validateIdcard (value: string): string | void {
    let exp = `^(\\d{6})(\\d{4})(\\d{2})(\\d{2})(\\d{3})([0-9]|X|x)$`
    return this.validateRegexp(value,exp,'身份证号格式不正确')
  }
}

export {
    resolveDir,
    logGreen,
    reactToStream,
    getVersion,
    ReadableString,
    UtilsMath,
    ValidateUtil
}
