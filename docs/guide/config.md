# 构建配置

这里我们存在baseConfig, clientConfig, serverConfig 三个配置文件，分别存放基础配置，客户端特有的配置，服务端特有的配置

## 基础配置

服务器端渲染(SSR)项目的配置大体上与纯客户端项目类似，我们建议将配置分为三个文件：base, client 和 server。基本配置(base config)包含在两个环境共享的配置，例如，resolve，plugins，module，别名(alias)和 loader等配置项。服务器配置(server config)和客户端配置(client config)，可以通过使用 webpack-merge 来简单地扩展基本配置。

我们利用 webpack 分别对客户端代码和服务器端代码分别进行打包，服务器需要服务器 bundle 用于服务器端渲染(SSR)，而客户端 bundle 会发送给浏览器，用于客户端对服务端渲染的 html 进行事件绑定和接管。

###  base config

