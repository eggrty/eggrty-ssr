---
sidebarDepth: 3
---

# HMR

现在每个框架的脚手架新建的项目基本都会提供[HMR](https://webpack.docschina.org/guides/hot-module-replacement/)功能。
这里首先区分两个概念，hot-module-replacement 以及 hot-module-reloading。两者的缩写都是 HMR，但前者叫热替换，后者叫热重载。

## 概念区分

在与其他开发者沟通的时候，我们发现他们经常将两个概念弄混，我表达的 HMR 是热替换，他们表达的 HMR 是热重载。

### 热替换

热替换，即修改代码后，浏览器无需刷新，而是通过局部替换来更新你的应用，很明显这样的开发体验更加的友好，但实现起来较为复杂

### 热重载

热重载，即代码修改后，自动刷新你的浏览器，无需你手动刷新。这种方式的体验没有热替换那么友好，但实现起来较为容易

## 热替换实现

这里我们介绍两种方式来实现，一种是[webpack-dev-server](https://webpack.docschina.org/configuration/dev-server/), 一种是直接用中间件的方式
现在最火的构建工具 webpack, 提供了 webpack-dev-server 这个工具，来帮你隐藏其中细节快速来实现热替换

### webpack-dev-server

webpack-dev-server 中内置了 express,在你本地开发时，它其实是用 express 创建了一个 Node Server，然后加载了[webpack-dev-middleware](https://github.com/webpack/webpack-dev-middleware)这个中间件，该中间件提供以下功能(来自于官网的介绍)

- 将文件打包在内存中，而不是打包到本地硬盘中
- 当启动了 webpack wacth 选项时，该中间件将延迟请求直到新的文件编译完成
- 支持热重载

光使用该中间件还无法实现热替换，webpack-dev-server 还使用了 sockjs 来实现热替换
使用方式:

```js
cross-env NODE_ENV=development webpack-dev-server --port 8000 --hot --config ./build/webpack.config.client.js
```

开启 --hot 选项即可，很多教程都说需要配置 new webpack.HotModuleReplacementPlugin 插件，其实当你开启 hot 选项的时候，webpack 已经自动帮你注入了这个插件，当你再重复添加时，会报栈溢出的错误，所以我们记住这里无需再额外手动添加 HotModuleReplacementPlugin 插件。

### CSS HMR 实现

大部分框架实现 css hmr 用的都是 style-loader,主要还是 style-loader 中使用了 module.hot.accept，在 CSS 依赖模块更新之后，会将其 patch(修补) 到 style 标签中。
但在该应用中，为了保持开发环境和生产环境的统一，因为在生产环境我们需要将 css 提取为单独文件，而不是以 style 标签的形式，所以这里我们没有用 style-loader,而是用 css-hot-loader
来实现 hmr。
参考 react-hot-loader 来实现一个 css-hot-loader 也不难。每次热加载都是一个 js 文件的修改，每个 css 文件在 webpack 中也是一个 js 模块，那么只需要在这个 css 文件对应的模块里面加一段代码就可以实现 css 文件的更新了。本质也是修改 css 的时候让浏览器去下载一个新的 css 文件

```js
let loaders = [
  {
    loader: require.resolve('css-hot-loader'),
  },
  {
    loader: MiniCssExtractPlugin.loader,
    options: Object.assign(
      {},
      shouldUseRelativeAssetPaths ? {publicPath: '../../'} : undefined
    ),
  },
  {
    loader: require.resolve('css-loader'),
    options: cssOptions,
  },
];
```
