# 介绍

该骨架是我们团队通过开发业务上的React SSR应用提取出来的一个基础的Egg + React + SSR 应用的实现，本文档会详细介绍一个完整的Egg + React + SSR 应用的开发流程, 我们力求该应用的实现方式比目前市面上任何框架的实现方式都要简单，同时做到本文档详细程度超过目前市面上的任何一篇文章并且涉及到一些源码讲解，不但教你如何做，还会教你为什么要这样做以及原理是什么。

## 初衷

## 技术栈

[Egg](https://eggjs.org/zh-cn/intro/index.html)企业级Node.js框架, 基于 Koa 开发，性能优异,高度可扩展的插件机制,内置多进程管理

[React](https://react.docschina.org/)目前最流行的前端框架之一

[ReactDOMServer](https://reactjs.org/docs/react-dom-server.html)React官方提供的服务端渲染有关的库

## 执行环境

- 服务器Node.js >= 7.6， 为了原生的使用async/await语法
- 浏览器版本大于等于IE9, React支持到IE9，但为了更好的在IE下使用，你可能需要引入[Polyfill](https://reactjs.org/docs/javascript-environment-requirements.html)

## 功能/特性

以下是该应用具有的功能点

### 已完成

- 基于cra脚手架开发，由cra开发的React App可无缝迁移，如果你熟悉cra的配置，上手成本几乎为0
- 小而美，相比于beidou，next.js这样的高度封装方案，我们的实现原理和开发模式一目了然
- 推荐使用egg作为Node.js框架但并不强制，事实上你可以发现几乎无需做任何修改即可迁移到koa,nest.js等框架
- 同时支持SSR以及CSR两种开发模式,本地开发环境以及线上环境皆可无缝切换两种渲染模式
- 统一前端路由与服务端路由，无需重复编写路由文件配置
- 支持切换路由时自动获取数据
- 支持本地开发HMR

