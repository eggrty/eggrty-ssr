module.exports = {
  title: 'Egg + React + TypeScripet = eggrty 服务端渲染',
  head: [
    ['link', { rel: 'icon', href: 'http://pic.51yuansu.com/pic3/cover/02/41/60/59e4e100dd4b4_610.jpg' }],
    ['meta', { name: 'keywords', itemprop: 'keywords', content: '服务端渲染, React, Egg, SSR, ReactSSR'}],
    ['meta', { property: 'og:title', content: 'Egg + React + TypeScripet = eggrty 服务端渲染'}],
    ['meta', { property: 'og:description', content: 'Egg + React + TypeScripet = eggrty 服务端渲染, 最小而美的服务端渲染应用骨架'}],
    ['script', {}, `var _hmt = _hmt || [];
    (function() {
      var hm = document.createElement("script");
      hm.src = "https://hm.baidu.com/hm.js?ad212e8d41079dc41abaeda9b36e2501";
      var s = document.getElementsByTagName("script")[0]; 
      s.parentNode.insertBefore(hm, s);
    })()`]
  ],
  locales: {
    '/': {
      lang: 'zh-CN',
      description: '最小而美的服务端渲染应用骨架',
    },
    '/en/': {
       // 英文版，再说啦
       lang: 'en-US'
    }
  },
  themeConfig: {
    locales: {
      '/': {
        nav: [
          { text: '指南', link: '/guide/' },
          { text: '配置', link: '/config/' },
          { text: 'Gitlab', link: 'https://gitlab.com/eggrty/eggrty' },
        ],
        selectText: '选择语言',
        label: '简体中文',
        sidebar: {
          '/guide/': [
            {
              collapsable: false,
              children: [
                '',
                'gettingStarted',
                'isomorphism',
                'config',
                'getInitialProps',
                'hydrate',
                'stream',
                'ssr-csr',
                'hmr',
                'optimize',
                'dev',
                'publish',
                'deploy',
                'loadable',
                'ts',
                'serverless',
                'thinking',
                'faq'
                // 'update'
              ],
            }
          ]
        }
      },
      '/en/': {
        selectText: 'Languages',
        label: 'English(待补充)'
      }
    }
  }
}
